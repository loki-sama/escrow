pragma solidity ^0.4.18;


/// @title Escrow
/// @author lucky
contract Escrow {
    uint public amount = 0;
    address public sender;
    address public reciever;

    function Escrow(address _reciever) public payable {
        sender = msg.sender;
        amount = msg.value;
        reciever = _reciever;
    }

    function reliceMoney() public {
        if (msg.sender == sender) {
            reciever.transfer(amount);
        }
    }
}
